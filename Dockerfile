FROM minizinc/minizinc:ubuntu_edge
WORKDIR /minizinc
RUN apt-get update && apt-get install -y libqt5printsupport5 wget nano
RUN wget https://github.com/google/or-tools/releases/download/v7.1/or-tools_flatzinc_ubuntu-18.04_v7.1.6720.tar.gz
RUN tar xvf or-tools_flatzinc_ubuntu-18.04_v7.1.6720.tar.gz
RUN mv or-tools_flatzinc_Ubuntu-18.04-64bit_v7.1.6720/bin/* /usr/local/bin
RUN mv or-tools_flatzinc_Ubuntu-18.04-64bit_v7.1.6720/lib/* /usr/local/lib
RUN mv or-tools_flatzinc_Ubuntu-18.04-64bit_v7.1.6720/share/* /usr/local/share
RUN rm -rf or-tools_flatzinc_Ubuntu-18.04-64bit_v7.1.6720
RUN rm or-tools_flatzinc_ubuntu-18.04_v7.1.6720.tar.gz
