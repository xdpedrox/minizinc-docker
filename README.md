# minizinc-docker

To start the docker image:

`docker-compose up -d`

run `./connect_to_image.sh` to use minizinc

Files inside the "files" folder are mounted in the work directory /minizinc

`ortools.msc` is mounted on the configuration folder (/usr/local/share/minizinc/solvers/) so it's easy to access and tweak.
